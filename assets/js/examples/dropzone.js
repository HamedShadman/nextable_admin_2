'use strict';

Dropzone.autoDiscover = false;

$(document).ready(function () {

    new Dropzone(".dropzone", {
		dictDefaultMessage:"فایل ها را برای ارسال اینجا بکشید",
		dictFallbackMessage:"Ù…Ø±ÙˆØ±Ú¯Ø± Ø´Ù…Ø§ Ø§Ø² Ú©Ø´ÛŒØ¯Ù† Ùˆ Ø±Ù‡Ø§ Ø³Ø§Ø²ÛŒ Ø¨Ø±Ø§ÛŒ Ø§Ø±Ø³Ø§Ù„ ÙØ§ÛŒÙ„ Ù¾Ø´ØªÛŒØ¨Ø§Ù†ÛŒ Ù†Ù…ÛŒ Ú©Ù†Ø¯.",
		dictFallbackText:"Ù„Ø·ÙØ§ Ø§Ø² ÙØ±Ù… Ø²ÛŒØ± Ø¨Ø±Ø§ÛŒ Ø§Ø±Ø³Ø§Ù„ ÙØ§ÛŒÙ„ Ù‡Ø§ÛŒ Ø®ÙˆØ¯ Ù…Ø§Ù†Ù†Ø¯ Ú¯Ø°Ø´ØªÙ‡ Ø§Ø³ØªÙØ§Ø¯Ù‡ Ú©Ù†ÛŒØ¯.",
		dictFileTooBig:"ÙØ§ÛŒÙ„ Ø®ÛŒÙ„ÛŒ Ø¨Ø²Ø±Ú¯ Ø§Ø³Øª ({{filesize}}MiB). Ø­Ø¯Ø§Ú©Ø«Ø± Ø§Ù†Ø¯Ø§Ø²Ù‡ Ù…Ø¬Ø§Ø²: {{maxFilesize}}MiB.",
		dictInvalidFileType:"Ø´Ù…Ø§ Ù…Ø¬Ø§Ø² Ø¨Ù‡ Ø§Ø±Ø³Ø§Ù„ Ø§ÛŒÙ† Ù†ÙˆØ¹ ÙØ§ÛŒÙ„ Ù†ÛŒØ³ØªÛŒØ¯.",
		dictResponseError:"Ø³Ø±ÙˆØ± Ø¨Ø§ Ú©Ø¯ {{statusCode}} Ù¾Ø§Ø³Ø® Ø¯Ø§Ø¯.",
		dictCancelUpload:"Ù„ØºÙˆ Ø§Ø±Ø³Ø§Ù„",
		dictUploadCanceled:"Ø§Ø±Ø³Ø§Ù„ Ù„ØºÙˆ Ø´Ø¯.",
		dictCancelUploadConfirmation:"Ø¢ÛŒØ§ Ø§Ø² Ù„ØºÙˆ Ø§ÛŒÙ† Ø§Ø±Ø³Ø§Ù„ Ø§Ø·Ù…ÛŒÙ†Ø§Ù† Ø¯Ø§Ø±ÛŒØ¯ØŸ",
		dictRemoveFile:"Ø­Ø°Ù ÙØ§ÛŒÙ„",
		dictRemoveFileConfirmation:"Ø¢ÛŒØ§ Ø§Ø² Ø­Ø°Ù Ø§ÛŒÙ† ÙØ§ÛŒÙ„ Ø§Ø·Ù…ÛŒÙ†Ø§Ù† Ø¯Ø§Ø±ÛŒØ¯ØŸ",
		dictMaxFilesExceeded:"Ø´Ù…Ø§ Ù†Ù…ÛŒ ØªÙˆØ§Ù†ÛŒØ¯ ÙØ§ÛŒÙ„ Ø¯ÛŒÚ¯Ø±ÛŒ Ø§Ø±Ø³Ø§Ù„ Ú©Ù†ÛŒØ¯."
	});

});